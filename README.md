## BotCom
A service that synchronizes a folder across 2 express servers on multiple devices.
<br>
### Introduction
----
This service requires an instance of the server the bot is running on to build on. All extra endpoints and middleware are handled by the service.
A test bot(as testBot.js) is provided out of the box which can be used. The configuration parameters are passed along with the express server instance to the main app.
This will be explained in detail in the configuration section.
### Installation
----
Simply clone the repository to the desired location. If you wish to test the service across a single machine, just
clone it into 2 separate locations. Run `npm i` to install the required dependencies.
```
git clone https://gitlab.com/Nano_Sec/BotCom
```
### Configuration
----
In the testBot.js file, edit the parameters as required.
- Path: By default, it is the data directory within the service. Remap it to any location you wish to. There's a sample text file in /data you can use to test the service.
- Companion Url: The url of the companion server that is to be synced.
- Companion Primary: The folder sync on initial boot is controlled by the primary bot. The initial sync re-runs if the primary bot comes back online
- Upsert: If this is true, then on initial sync, secondary server files which are not on primary will be copied to it. If not, they will be deleted 
- Port: The port that the testBot runs on.
### Usage
----
Here's an example: 
<br>
- Bot 1: default params in testBot.js
- Bot 2:
```
var path                = `${__dirname}/data/`
var companionUrl        = 'http://localhost:3000'
var companionPrimary    = false
var upsert              = true
var port                = 3002
```
Note that the companion Url's match the ports that each bots are running on.
Run both servers:
```
npm start
```
As soon as both of them find each other, the initial sync should begin.
<br>
And that's it! Any changes made to files on one should appear on the other.
### Known bugs
----
- Watcher creates new files in base directory regardless of folder depth. (recursive mapping in next release)