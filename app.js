const bot  = require('./testBot')

// Configurables
global.path             = bot.path
global.companionUrl     = bot.companionUrl
global.companionPrimary = bot.companionPrimary
global.upsert           = bot.upsert

// Initial Configuration
require('./config/initialConfig')

// External Libraries
const bodyParser  = require('body-parser')
const serveStatic = require('serve-static')

// Internal Libraries
const dataService = require('./services/dataService')

// Middleware
bot.server.use(bodyParser.json())
bot.server.use(serveStatic(global.path, {index: global.index}))

// CORS headers
bot.server.all('*', (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200')
    next()
})

// Endpoints
bot.server.get('/ping', (req, res) => {
    if(global.companionPrimary != undefined || global.companionPrimary != null)
        res.status(200).send({companionPrimary: global.companionPrimary})
    else 
        res.status(500).send('Companion primary not configured')
})

bot.server.get('/data', async (req, res) => {
    try {
        var data    = await dataService.collectFolderData()
        var stats   = await dataService.getStats(data)
    } catch (error) {
        console.log(error)
        res.status(500).send(error)
        return
    }
    res.status(200).send({files: data, stats: stats})
})

bot.server.post('/fetch', async (req, res) => {
    try {
        if(req.body instanceof Array)
            await dataService.downloadFiles(req.body)
        else {
            res.status(400).send('Expected Array of files')
            return
        }
    } catch (error) {
        console.log(error)
        res.status(500).send(error)
        return
    }
    res.status(204).send()
})

bot.server.delete('/remove', async (req, res) => {
    try {
        if(req.body instanceof Array)
            await dataService.deleteFiles(req.body)
        else {
            res.status(400).send('Expected Array of files')
            return
        }
    } catch (error) {
        console.log(error)
        res.status(500).send(error)
        return
    }
    res.status(204).send()
})