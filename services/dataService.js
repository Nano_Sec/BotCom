// External Libraries
const fs    = require('fs')
const axios = require('axios')

var startupSync = async () => {
    var data            = await collectFolderData()
    global.index        = data
    if(global.companionPrimary) {
        var stats           = await getStats(data)
        var companionData   = await getCompanionData()
        await  updateFolders({files: data, stats: stats}, companionData)
    }
}

var collectFolderData = async () => {
    return new Promise(async (resolve, reject) => {
        fs.readdir(global.path, (error, items) => {
            if(error) {
                console.log('Please specify a valid data path')
                process.exit()
            } else {
                resolve(items)
            }
        })
    })
}

var getCompanionData = async () => {
    try {
        var response = await axios.get(global.companionUrl + '/data')
        return response.data
    } catch (error) {
        console.log(error)
    }
}

var getStats = (data) => {
    return new Promise((resolve, reject) => {
        var stats = []
        for(let file in data) {
            fs.stat(global.path + '/' + data[file], (error, stat) => {
                if(error) {
                    console.log('Error getting stats of: ' + data[file])
                    reject(data[file])
                } else {
                    stats.push(stat)
                }
            })
        }
        setTimeout(() => {resolve(stats)}, 1000)
    })
}

var updateFolders = async (localData, companionData) => {
    try {
        var lists = await generateLists(localData, companionData)
        if(typeof lists.fetchFromCompanion !== 'undefined' && lists.fetchFromCompanion.length > 0)
            downloadFiles(lists.fetchFromCompanion)
        if(typeof lists.askCompanionToFetch !== 'undefined' && lists.askCompanionToFetch.length > 0)
            sendFetchList(lists.askCompanionToFetch)
        if(typeof lists.askCompanionToDelete !== 'undefined' && lists.askCompanionToDelete.length > 0)
            sendDeleteList(lists.askCompanionToDelete)   
    } catch (error) {
        console.log(error)
    }
    console.log('Startup sync complete!')
}

var generateLists = async (localData, companionData) => {
    let fetchFromCompanion  = []
    let askCompanionToFetch = []
    let askCompanionToDelete = []
    for(let file in localData.files) {
        if(companionData.files.includes(localData.files[file])) {
            let companionFile = companionData.files.findIndex(i => i == localData.files[file])
            if(localData.stats[file].mtimeMs > companionData.stats[companionFile].mtimeMs)
                askCompanionToFetch.push(localData.files[file])
            else 
                fetchFromCompanion.push(localData.files[file])
        } else {
            askCompanionToFetch.push(localData.files[file])
        }
    }
    for(let file of companionData.files) {
        if(!localData.files.includes(file)) {
            if(!global.upsert)
                askCompanionToDelete.push(file)
            else
                fetchFromCompanion.push(file)
        }
    }
    return ({fetchFromCompanion: fetchFromCompanion,
        askCompanionToFetch: askCompanionToFetch,
        askCompanionToDelete: askCompanionToDelete})
}

var downloadFiles = async (files) => {
    for(let file of files) {
        await downloadFile(file)
    }
}

var downloadFile = async (file) => {
    try {
        var response = await axios({
            method: 'GET',
            url: global.companionUrl + '/' + file,
            responseType: 'stream'
        })
        response.data.pipe(fs.createWriteStream(global.path + '/' + file))

    } catch (error) {
        console.log(error)
    }
}

var sendFetchList = async (files) => {
    try {
        var response = await axios.post(global.companionUrl + '/fetch', files)
    } catch (error) {
        console.log(error)
    }
}

var sendDeleteList = async (files) => {
    try {
        var response = await axios.delete(global.companionUrl + '/remove', {data: files})
    } catch (error) {
        console.log(error)
    }
}

var deleteFiles = async (files) => {
    return new Promise(async (resolve, reject) => {
        for(let file in files) {
            fs.unlink(global.path + '/' + files[file], (error) => {
                if (error) {
                    console.log('Unable to delete file: ' + files[file])
                    reject()
                }
                console.log(files[file] + ' was deleted')
            })
        }
        setTimeout(() => {resolve()}, 1000)
    })
}

module.exports = {
    startupSync,
    collectFolderData,
    getStats,
    downloadFiles,
    deleteFiles,
    sendFetchList,
    sendDeleteList
}