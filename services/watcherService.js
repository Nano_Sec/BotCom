// External Libraries
const chokidar      = require('chokidar')
const path          = require('path')

// Internal Libraries
const dataService   = require('./dataService')

var startWatcher = async () => {
    var watcher = chokidar.watch(global.path, {
        ignored: /(^|[\/\\])\../,
        persistent: true
    })

    watcher
        .on('add', pathName => {
            console.log(`File ${path.basename(pathName)} has been added`)
            dataService.sendFetchList([path.basename(pathName)])
        })
        .on('change', pathName => {
            console.log(`File ${path.basename(pathName)} has been changed`)
            dataService.sendFetchList([path.basename(pathName)])
        })
        .on('unlink', pathName => {
            console.log(`File ${path.basename(pathName)} has been removed`)
            dataService.sendDeleteList([path.basename(pathName)])
        })
}

module.exports = {
    startWatcher
}