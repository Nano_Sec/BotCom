// @Configurables
var path                = `${__dirname}/data/`
var companionUrl        = 'http://localhost:3002'
var companionPrimary    = true
var upsert              = true
var port                = 3000

// External Libraries
const express       = require('express')

// Server allocation
const server = express()

server.get('/', (req, res) => {
    res.status(200).send({'bot': 'running'})
})

server.listen(process.env.PORT || port, () => {
    console.log('bot started at port ' + port)
})

module.exports = {
    server,
    path,
    companionUrl,
    companionPrimary,
    upsert
}