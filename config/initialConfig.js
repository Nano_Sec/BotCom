const path              = require('path')
const axios             = require('axios') 
const dataService       = require('../services/dataService')
const watcherService    = require('../services/watcherService')

if(!path.isAbsolute(global.path)) {
    console.log('Provide a valid absolute data path')
    process.exit()
} else {
    console.log('Attempting to contact companion..')
}

var ping = setInterval(async () => {
    try {
        var response = await axios.get(global.companionUrl + '/ping')
        if(response.data) {
            console.log('Success!')
            checkPrimaryStatus(response.data)
            clearInterval(ping)
        }
    } catch (error) {}
}, 5000)

var checkPrimaryStatus = async (data) => {
    if(data.companionPrimary == global.companionPrimary) {
        console.log('One bot should be primary while the other shouldn\'t')
        process.exit()
    } else {
        if(global.companionPrimary)
            console.log('Initiating Startup sync...')
        await dataService.startupSync()
        await watcherService.startWatcher()
    }
}

